﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Helper class which can move and turn the object
/// to a epcific point and direction
/// </summary>
public class GameObjectMover: MonoBehaviour
{
    /// <summary>
    /// Event, which is called, when the move is done
    /// </summary>
    public event EventHandler MoveDone;
    /// <summary>
    /// Event, which is called, when the turn is done
    /// </summary>
    public event EventHandler TurnDone;
    /// <summary>
    /// Event, which is called, when the transform is done
    /// </summary>
    public event EventHandler TransformDone;

    /// <summary>
    /// Transforms the MonoBehaviour to the new position facing to the new direction
    /// </summary>
    /// <param name="to">The new position, where the Monobehaviour will be moved to</param>
    /// <param name="direction">The new direction where the Monobehaviour will face toward</param>
    /// <param name="duration">The duration, in seconds, for the transformation</param>
    /// <param name="space">Should the transformation happen in Local or Global space?</param>
    public void Transform(Vector3 to, Vector3 direction, float duration, Space space)
    {
        Move(to, duration, space, null);
        Turn(direction, duration, space, TransformDone);
    }

    /// <summary>
    /// Moves the MonoBehaviour to the new position
    /// </summary>
    /// <param name="to"><see cref="Transform(Vector3, Vector3, float, Space)"/></param>
    /// <param name="duration"><see cref="Transform(Vector3, Vector3, float, Space)"/></param>
    /// <param name="space"><see cref="Transform(Vector3, Vector3, float, Space)"/></param>
	public void Move(Vector3 to, float duration, Space space)
    {
        Move(to, duration, space, MoveDone);
    }

    /// <summary>
    /// Turnes the MonoBehaviour to the new direction
    /// </summary>
    /// <param name="direction"><see cref="Transform(Vector3, Vector3, float, Space)"/></param>
    /// <param name="duration"><see cref="Transform(Vector3, Vector3, float, Space)"/></param>
    /// <param name="space"><see cref="Transform(Vector3, Vector3, float, Space)"/></param>
    public void Turn(Vector3 direction, float duration, Space space)
    {
        Turn(direction, duration, space, TurnDone);
    }

    private void Move(Vector3 to, float duration, Space space, EventHandler eventHandler)
    {
        Func<Vector3> from = null;
        Action<Vector3> set = null;

        // Sanity check
        if (duration <= 0)
            throw new ArgumentException("Duration cannot be zero or negative", "duration");
        switch (space)
        {
            case Space.World:
                from = () => transform.position;
                set = position => transform.position = position;
                break;
            case Space.Self:
                from = () => transform.localPosition;
                set = position => transform.localPosition = position;
                break;
            default:
                throw new ArgumentException("Unknown enum value", "space");
        }
        StartCoroutine(TurnOrMove(from, () => to, set, Vector3.Lerp, duration, eventHandler));
    }

    private void Turn(Vector3 direction, float duration, Space space, EventHandler eventHandler)
    {
        Quaternion to = Quaternion.LookRotation(direction);
        Func<Quaternion> from = null;
        Action<Quaternion> set = null;

        // Sanity check
        if (duration <= 0)
            throw new ArgumentException("Duration cannot be zero or negative", "duration");
        switch (space)
        {
            case Space.World:
                from = () => transform.rotation;
                set = rotation => transform.rotation = rotation;
                break;
            case Space.Self:
                from = () => transform.localRotation;
                set = rotation => transform.localRotation = rotation;
                break;
            default:
                throw new ArgumentException("Unknown enum value", "space");
        }
        StartCoroutine(TurnOrMove(from, () => to, set, Quaternion.Lerp, duration, eventHandler));
    }

    private IEnumerator TurnOrMove<T>(Func<T> from, Func<T> to, Action<T> set, Func<T, T, float, T> lerp, float duration, EventHandler eventHandler)
    {
        float startTime = Time.time;
        float factor = 0;
        T fromT = from();

        while (factor < 1)
        {
            factor = (Time.time - startTime) / duration;
            set(lerp(fromT, to(), factor));

            yield return null;
        }
        if (null != eventHandler)
            eventHandler(this, EventArgs.Empty);
    }
}
