﻿using UnityEngine;
using System.Collections.Generic;

public partial class AiVehicleController : VehicleController
{
    private class Node
    {
        public Vector2 Position { get; set; }
        public Vector2 Speed { get; set; }
        public LevelGenerator LevelGenerator { get; set; }
        public Node Parent { get; set; }

        public List<Node> GetNexts(int layerMask)
        {
            List<Vector2> validPositions = GetValidPositions(layerMask);
            List<Node> nexts = new List<Node>();

            for(int i = 0; i < validPositions.Count; ++i)
            {
                Node node = new Node() { Position = validPositions[i], Speed = validPositions[i] - Position, LevelGenerator = LevelGenerator, Parent = this };

                nexts.Add(node);
            }

            return nexts;
        }

        public override bool Equals(object other)
        {
            Node node = other as Node;

            if (null == node)
                return false;
            return Position == node.Position && Speed == node.Speed;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        private List<Vector2> GetValidPositions(int layerMask)
        {
            List<Vector2> validPoints = new List<Vector2>(9);
            Vector2 position = Position + Speed;
            Vector3 position3d = ConvertToVector3(Position);

            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    Vector2 neighbour = new Vector2(i, j) + position;

                    if (LevelGenerator[neighbour])
                    {
                        // TODO: By sometimes "forgetting" raycasting here, we can have more "dumb" vehicles, which
                        //       will crach sometimes, os choose not optimal ways. With this we can have different difficulties
                        Vector3 distance = ConvertToVector3(neighbour) - position3d;

                        // Check if there is any obstacle in the way to reach the valid position
                        if (BoxCast(position3d, distance, layerMask))
                            continue;
                        validPoints.Add(neighbour);
                    }
                }
            }

            return validPoints;
        }
    }
}
