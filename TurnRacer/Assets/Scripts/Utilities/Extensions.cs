﻿using UnityEngine;
using System.Collections.Generic;

public static class Extensions
{
    public static T Last<T>(this IList<T> list)
    {
        return Last(list, 0);
    }

    public static T Last<T>(this IList<T> list, int index)
    {
        return list[list.Count - 1 - index];
    }

    public static Vector2 ToVector2(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.z);
    }
}
