﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class TurnManager
{
    public int Id { get; private set; }
    public bool KeepOn { get; set; }
    public event EventHandler TurnStarted;
    public event EventHandler TurnEnded;
    public event EventHandler RaceEnded;
    public event EventHandler VehicleChrashed;
    public event EventHandler CheckpointReached;

    public TurnManager(int id, CheckpointController checkpoint, VehicleController vehicle)
    {
        Id = id;
        CheckPointController = checkpoint;
        VehicleController = vehicle;
        CanvasController.Instance.TurnManagerCreated(this);
        KeepOn = false;
    }

    public bool Initialize(Vector3 startPosition, Vector3[] checkpoints)
    {
        // Creating the vehicle
        Vehicle = GameObject.Instantiate(VehicleController);
        if (!Vehicle.init(Id, startPosition, checkpoints))
            return false;
        Vehicle.enabled = false;
        Vehicle.TurnEnded += VehicleTurnEnded;
        Vehicle.VehicleChrashed += VehicleVehicleChrashed;
        // Setting up checkpoints
        Checkpoints = new List<CheckpointController>(4);
        CreateCheckpoint(startPosition, true);
        for (int i = 0; i < checkpoints.Length; ++i)
        {
            CreateCheckpoint(checkpoints[i], false);
        }

        return true;
    }

    public void StartTurn()
    {
        Switch(Turning.ON);
        SendEvent(TurnStarted);
    }

    public VehicleController GetVehicleController()
    {
        return Vehicle.GetComponent<VehicleController>();
    }

    private void CreateCheckpoint(Vector3 position, bool startPosition)
    {
        CheckpointController checkpoint = GameObject.Instantiate(CheckPointController);
        VehicleController controller = GetVehicleController();

        checkpoint.transform.position = position;
        checkpoint.init(Id, startPosition, controller.LightColor);
        checkpoint.gameObject.SetActive(false);
        checkpoint.CheckpointReached += CheckpointCheckpointReached;
        Checkpoints.Add(checkpoint);
    }

    private void CheckpointCheckpointReached(object sender, EventArgs eventArgs)
    {
        CheckpointController controller = sender as CheckpointController;

        if (null == controller)
        {
            Debug.LogError("Invalid event received from CheckPointController!!!");
            return;
        }
        Checkpoints.Remove(controller);
        SendEvent(CheckpointReached);
        if (Checkpoints.Count == 0)
            SendEvent(RaceEnded);
    }

    private void VehicleTurnEnded(object sender, EventArgs eventArgs)
    {
        Switch(Turning.OFF);
        SendEvent(TurnEnded);
    }

    private void VehicleVehicleChrashed(object sender, EventArgs eventArgs)
    {
        SendEvent(VehicleChrashed);
        Switch(Turning.OFF);
        if (!KeepOn)
        {
            // Destroy the remaining checkpoints - the Vehicle is already destroyed
            for (int i = 0; i < Checkpoints.Count; ++i)
            {
                GameObject.Destroy(Checkpoints[i].gameObject);
            }
            Checkpoints.Clear();
        }
    }

    private void SendEvent(EventHandler eventHandler)
    {
        if (null != eventHandler)
            eventHandler(this, EventArgs.Empty);
    }

    private enum Turning
    {
        ON,
        OFF,
    }

    private void Switch(Turning turn)
    {
        // If the KeepOn mmber is ON, we should not turn anythong off
        if (Turning.OFF == turn && KeepOn)
            return;
        for (int i = 0; i < Checkpoints.Count; ++i)
        {
            Checkpoints[i].gameObject.SetActive(Turning.ON == turn);
        }
        Vehicle.enabled = Turning.ON == turn;
    }

    private CheckpointController CheckPointController { get; set; }
    private VehicleController VehicleController { get; set; }
    private VehicleController Vehicle { get; set; }
    private List<CheckpointController> Checkpoints { get; set; }
}
