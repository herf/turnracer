﻿using UnityEngine;
using System.Collections.Generic;

// TODO: When everything is ready, make the level generation more interesting,
//       by not doing everything in the Start(), but doing one small step in each frame
//       until it is ready, and show the intermediatee step to the user through a camera,
//       which is rotating around the level
// TODO: Use the library classes i created for user input and timing event handling
// TODO: "Tiling" in the substance is not perfect, since it loses the details, because it just compresses
//       the texture, into  aportion of it. Somehow we need to achive the same tiling as in unity,
//       bt the mask should not be tiled
// TODO: Forget the options about different level sizes, use only the small one.
//       With this we can achieve better performance, since more thing can be made statis,
//       And maybe there is a better way to do the tiling in SUbstance designer for a certain material
// TODO: Add sounds
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class LevelCreator: SingletonMonoBehaviour<LevelCreator>
{
    public LevelGenerator.Size Size = LevelGenerator.Size.SMALL;
    public ProceduralMaterial LevelMaterial = null;
    public ProceduralMaterial LevelSideMaterial = null;
    public LevelSphereController LevelSphere = null;
    public UserVehicleController UserVehicle = null;
    public AiVehicleController AiVehicle = null;
    public CheckpointController CheckPoint = null;
    public ParticleSystem RainingFire = null;
    public ParticleSystem DustStorm = null;
    public GameObject Flag = null;
    public CameraController CameraController = null;
    // Properties
    public LevelGenerator LevelGenerator { get; protected set; }
    public LevelSphereGenerator LevelSphereGenerator { get; protected set; }
    public GameManager GameManager { get; protected set; }

    void Start()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        int tries = 0;

        // Generate the level data
        do
        {
            Debug.Log("Generating level");
            LevelGenerator = new LevelGenerator(Size);
        }
        while (!IsLevelPlayable() && tries++ < 10);
        // Generate the mesh, representing the level
        LevelMeshGenerator = new LevelMeshGenerator();
        meshFilter.mesh = LevelMeshGenerator.LevelMesh;
        gameObject.AddComponent<MeshCollider>();
        // Set up the substance and rebuild the textures immediately
        LevelMaterial.SetProceduralTexture("Mask", LevelGenerator.LevelMask);
        LevelMaterial.SetProceduralFloat("Tiling", LevelGenerator.Tiling);
        LevelSideMaterial.SetProceduralFloat("Tiling", LevelGenerator.Tiling);
        LevelMaterial.RebuildTexturesImmediately();
        meshRenderer.material = LevelMaterial;
        // Generate the spheres showing the path to the player
        LevelSphereGenerator = new LevelSphereGenerator(LevelSphere);
        PlaceFlags();
        PlaceParticle();
        PlaceHoleColliders();
        // Time to start the game
        GameManager = new GameManager(UserVehicle, AiVehicle, CheckPoint, CreateCameraController());
        GameManager.StartGame();
        // Set the resolution to the final quality, but this time load it in the background
        LevelMaterial.SetProceduralVector("$outputsize", LevelGenerator.MaterialOutputSize);
        LevelMaterial.RebuildTextures();
        LevelSideMaterial.SetProceduralVector("$outputsize", LevelGenerator.MaterialOutputSize);
        LevelSideMaterial.RebuildTextures();
    }

    protected override void onAwake()
    {
        // Nothing to do yet
    }

    protected override void onDestroy()
    {
        // Nothing to do yet
    }

    protected override LevelCreator getSingletonInstance()
    {
        return this;
    }

    protected void PlaceParticle()
    {
        int middle = LevelGenerator.LevelSize / 2;
        int size = (int)Size;
        ParticleSystem rainingFire = Instantiate(RainingFire);
        ParticleSystem dustStorm = Instantiate(DustStorm);
        var rainingFireEmission = rainingFire.emission;
        var dustStormEmission = dustStorm.emission;
        var rainingFireBox = rainingFire.shape;
        var dustStormBox = dustStorm.shape;
        // TODO: This is not really generic, it will not work with other sizes, just with SMALL
        // The scaling of the emission rate, based on the level size
        // If the level size is doubled, the emission rate has to be 4 times bigger
        // to have the same amount of particles in each level size
        // Multiplying it by 9 since it will be over nine tiles, not just the level
        float emissionRateScale = Mathf.Pow(2, 2 * size) * 9;
        // The scaling of the particle system shape, based on the level size
        // Multiplying it by 3 since it will be over nine tiles, not just the level
        float boxScale = Mathf.Pow(2, size) * 3;

        // Adjusting the particle system based on the level size
        rainingFire.transform.position = new Vector3(middle, 10, middle);
        rainingFireBox.box *= boxScale;
        dustStorm.transform.position = new Vector3(middle, 0, middle);
        dustStormBox.box *= boxScale;
        rainingFireEmission.rate = new ParticleSystem.MinMaxCurve(rainingFireEmission.rate.constantMax * emissionRateScale);
        dustStormEmission.rate = new ParticleSystem.MinMaxCurve(dustStormEmission.rate.constantMax * emissionRateScale);
    }

    protected void PlaceFlags()
    {
        int size = LevelGenerator.LevelSize;
        int middle = size / 2;
        Vector3 position = GetRandomPosition(1, middle, 1, middle);
        Vector3 direction = LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_LEFT) -
                            LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_LEFT);

        Instantiate(Flag, position, Quaternion.LookRotation(direction));
        position = GetRandomPosition(1, middle, middle, size);
        direction = LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_LEFT) -
                    LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_RIGHT);
        Instantiate(Flag, position, Quaternion.LookRotation(direction));
        position = GetRandomPosition(middle, size, middle, size);
        direction = LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_RIGHT) -
                    LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_RIGHT);
        Instantiate(Flag, position, Quaternion.LookRotation(direction));
        position = GetRandomPosition(middle, size, 1, middle);
        direction = LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_RIGHT) -
                    LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_LEFT);
        Instantiate(Flag, position, Quaternion.LookRotation(direction));
    }

    protected CameraController CreateCameraController()
    {
        int middle = LevelGenerator.LevelSize / 2;
        CameraController cameraController = Instantiate(CameraController);

        cameraController.transform.position = new Vector3(middle, middle, middle);
        cameraController.transform.Rotate(new Vector3(60, 0, 0));

        return cameraController;
    }

    private bool IsLevelPlayable()
    {
        // The level is considered playable, if there is a walkable path around the level
        PathFinder pathFinder = new PathFinder(LevelGenerator);
        List<Vector2> path = null;

        if (!GetPath(pathFinder, LevelGenerator.Quarter.BOTTOM_LEFT, LevelGenerator.Quarter.TOP_LEFT, out path))
            return false;
        if (!GetPath(pathFinder, LevelGenerator.Quarter.TOP_LEFT, LevelGenerator.Quarter.TOP_RIGHT, out path))
            return false;
        if (!GetPath(pathFinder, LevelGenerator.Quarter.TOP_RIGHT, LevelGenerator.Quarter.BOTTOM_RIGHT, out path))
            return false;
        if (!GetPath(pathFinder, LevelGenerator.Quarter.BOTTOM_RIGHT, LevelGenerator.Quarter.BOTTOM_LEFT, out path))
            return false;

        return true;
    }

    private bool GetPath(PathFinder pathFinder, LevelGenerator.Quarter from, LevelGenerator.Quarter to, out List<Vector2> path)
    {
        Vector3 fromPosition = LevelGenerator.GetStartingPoint(from);
        Vector3 toPosition = LevelGenerator.GetStartingPoint(to);

        path = pathFinder.getPath(fromPosition.ToVector2(), toPosition.ToVector2());
        if (0 == path.Count)
            return false;

        return true;
    }

    private Vector3 GetRandomPosition(int fromX, int  toX, int fromY, int toY)
    {
        int x = 0;
        int y = 0;
        int tries = 0;

        do
        {
            x = Random.Range(fromX, toX);
            y = Random.Range(fromY, toY);
        }
        while (LevelGenerator.GetLevelAt(x, y) != LevelGenerator.Level.GROUND && tries++ < 10);

        return new Vector3(x, 0, y);
    }

    private void PlaceHoleColliders()
    {
        int size = LevelGenerator.LevelSize;
        GameObject parent = new GameObject();

        parent.transform.position = Vector3.zero;
        parent.name = "Hole Colliders";
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                if(LevelGenerator.Level.HOLE == LevelGenerator.GetLevelAt(i, j))
                {
                    GameObject go = new GameObject();
                    BoxCollider collider = go.AddComponent<BoxCollider>();

                    go.transform.position = VehicleController.ConvertToVector3(new Vector2(i, j));
                    collider.size = Vector3.one / 2;
                    go.name = "Hole Collider";
                    go.transform.parent = parent.transform;
                }
            }
        }           
    }

    protected LevelMeshGenerator LevelMeshGenerator { get; set; }
}
