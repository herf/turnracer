﻿using UnityEngine;

public class LevelGenerator
{
    public enum Size
    {
        SMALL = 0,
        MEDIUM = 1,
        LARGE = 2,
    }

    public enum Quarter
    {
        TOP_LEFT = 0,
        TOP_RIGHT = 1,
        BOTTOM_LEFT = 2,
        BOTTOM_RIGHT = 3,
    }

    public enum Level
    {
        GROUND,
        ROAD,
        HOLE,
    }

    public int LevelSize { get; private set; }
    public int Tiling { get; private set; }
    public Texture2D LevelMask { get; private set; }
    public Vector4 MaterialOutputSize { get; private set; }

    public LevelGenerator(Size levelSize)
    {
        int size = (int)levelSize;

        _size = levelSize;
        // Calculate the size by simple bit shifting
        // SMALL -> 32x32
        // MEDIUM -> 64x64
        // LARGE -> 128x128
        // But in fact, the number of vertices, we need to deal with is a one plus
        // e.g.: To create a 2x2 mesh, we need in total 9 vertices (3x3)
        LevelSize = (32 << size) + 1;
        // Same  method to salculate the tiling based on the size - but no plus one
        Tiling = (4 << size);
        // The material output size is also dependent on the level size
        // 10 means 1024x1024, 11 means 2048x2048 and 12 is 4096x4096
        MaterialOutputSize = new Vector4(10 + size, 10 + size);
        LevelData = new Level[LevelSize, LevelSize];
        GenerateLevel();
        GenerateMask();
    }

    public bool this[int x, int y]
    {
        get
        {
            if (x < 0 || x >= LevelData.GetLength(0) || y < 0 || y >= LevelData.GetLength(1))
                return false;
            return Level.ROAD == LevelData[x, y];
        }
    }

    public bool this[Vector2 coordinates]
    {
        get { return this[(int)coordinates.x, (int)coordinates.y]; }
    }

    public bool this[Vector3 coordinates]
    {
        get { return this[new Vector2(coordinates.x, coordinates.z)]; }
    }

    public Level GetLevelAt(int x, int y)
    {
        if (x < 0 || x >= LevelData.GetLength(0) || y < 0 || y >= LevelData.GetLength(1))
            return Level.GROUND;
        else return LevelData[x, y];
    }

    public Vector3 GetStartingPoint(Quarter quarter)
    {
        return _startingPoints[(int)quarter];
    }

    private void GenerateMask()
    {
        int size = LevelSize - 1;

        LevelMask = new Texture2D(size * 2, size * 2);
        // Set the whole texture white - meaning the ground material
        for (int x = 0; x < size * 2; ++x)
        {
            for (int y = 0; y < size * 2; ++y)
            {
                LevelMask.SetPixel(x, y, Color.white);
            }
        }
        for (int x = 0; x < size; ++x)
        {
            for (int y = 0; y < size; ++y)
            {
                int maskX = 2 * x;
                int maskY = 2 * y;

                if (this[x, y])
                    LevelMask.SetPixel(maskX, maskY, Color.black);
                if (this[x + 1, y])
                    LevelMask.SetPixel(maskX + 1, maskY, Color.black);
                if (this[x, y + 1])
                    LevelMask.SetPixel(maskX, maskY + 1, Color.black);
                if (this[x + 1, y + 1])
                    LevelMask.SetPixel(maskX + 1, maskY + 1, Color.black);
            }
        }
        LevelMask.Apply();
    }

    private void GenerateLevel()
    {
        int size = LevelSize - 2;
        int middle = size / 2;
        // Calculate random points in each quarter of the level
        Vector2 topLeft = new Vector2(Random.Range(1, middle), Random.Range(middle, size));
        Vector2 topRight = new Vector2(Random.Range(middle, size), Random.Range(middle, size));
        Vector2 bottomLeft = new Vector2(Random.Range(1, middle), Random.Range(1, middle));
        Vector2 bottomRight = new Vector2(Random.Range(middle, size), Random.Range(1, middle));
        // The number of iterations is dependent on the size
        //   Small: 8, Medium: 24, Large: 32
        int iterations = 16 + (int)_size * 8;

        _startingPoints[(int)Quarter.TOP_LEFT] = VehicleController.ConvertToVector3(topLeft);
        _startingPoints[(int)Quarter.TOP_RIGHT] = VehicleController.ConvertToVector3(topRight);
        _startingPoints[(int)Quarter.BOTTOM_LEFT] = VehicleController.ConvertToVector3(bottomLeft);
        _startingPoints[(int)Quarter.BOTTOM_RIGHT] = VehicleController.ConvertToVector3(bottomRight);
        // Make paths between the starting points, so we can make sure it will form a kind of circle
        for (int i = 0; i < iterations; ++i)
        {
            GeneratePath(topLeft, topRight);
            GeneratePath(topRight, bottomRight);
            GeneratePath(bottomRight, bottomLeft);
            GeneratePath(bottomLeft, topLeft);
        }
        // Make sure the starting points are valid ones
        SetPath(topLeft, justRoad: true);
        SetPath(topRight, justRoad: true);
        SetPath(bottomLeft, justRoad: true);
        SetPath(bottomRight, justRoad: true);
    }

    private void GeneratePath(Vector2 from, Vector2 to)
    {
        Vector2 currentPosition = from;
        int counter = 0;

        while (currentPosition != to)
        {
            // Random number with possible values: -1, 0, 1
            int randomX = Random.Range(-1, 2);
            int randomY = Random.Range(-1, 2);
            // Vector  pointon to the destination
            Vector2 direction = to - currentPosition;

            direction.Normalize();
            SetPath(currentPosition);
            // Calculate the next position, but influence it with the direction towards the destination
            currentPosition += new Vector2(randomX, randomY) + direction;
            if (100 == counter++)
                break;
        }
    }

    private void SetPath(Vector2 coordinates, bool justRoad = false)
    {
        int size = LevelSize - 2;
        int x = (int)coordinates.x;
        int y = (int)coordinates.y;

        // Normalize te coordinates, so they are inside the level
        x = Mathf.Max(1, x);
        x = Mathf.Min(x, size);
        y = Mathf.Max(1, y);
        y = Mathf.Min(y, size);
        // Sometimes, instead of road, create a hole
        // But holes can only be created when all the neighbours are road
        if(CheckNeighbours(x, y, Level.ROAD))
        {
            if (justRoad || Random.Range(0, 100) < 90)
                LevelData[x, y] = Level.ROAD;
            else
                LevelData[x, y] = Level.HOLE;
        }
        else
        {
            LevelData[x, y] = Level.ROAD;
        }
    }

    private bool CheckNeighbours(int x, int y)
    {
        return CheckNeighbours(x, y, GetLevelAt(x, y));
    }

    private bool CheckNeighbours(int x, int y, Level level)
    {
        for(int i = -1; i < 2; ++i)
        {
            for(int j = -1; j < 2; ++j)
            {
                if (GetLevelAt(x + i, y + j) != level)
                    return false;
            }
        }

        return true;
    }

    private Size _size = Size.SMALL;
    private Vector3[] _startingPoints = new Vector3[4];
    private Level[,] LevelData { get; set; }
}
