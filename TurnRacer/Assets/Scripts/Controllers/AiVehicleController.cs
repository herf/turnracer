﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

// TODO: Make every class Unit testable, by e.g. using virtual fields
public partial class AiVehicleController : VehicleController
{
    #region public methods
    public override bool init(int id, Vector3 startPosition, Vector3[] checkpoints)
    {
        // Everything but the checkpoints and vehicles, since vehicles starts at the checkspoints
        int layerMask = ~(1 << 8 | 1 << 9);

        if (!base.init(id, startPosition, checkpoints))
            return false;
        Checkpoints = new List<Vector2>(checkpoints.Length + 1);
        for (int i = 0; i < checkpoints.Length; ++i)
            Checkpoints.Add(checkpoints[i].ToVector2());
        Checkpoints.Add(transform.position.ToVector2());
        return setPath(layerMask);
    }
    #endregion

    #region unity methods
    private void OnEnable()
    {
        EnableTime = Time.time;
    }

	private void Update()
    {
        switch (mState)
        {
            case State.IDLE:
                turnOnSpheres();
                if (Time.time > EnableTime + 1)
                {
                    Vector2 nextStep = Path[++mCurrentPathIndex];

                    // Time to move
                    turnOffSpheres();
                    if(!mValidPoints.Contains(nextStep))
                    {
                        // Everything but the checkpoints
                        int layerMask = ~(1 << 9);

                        // TODO: What to do, if a car sits on the next CheckPoint?
                        Debug.LogError("Cannot choose the next path element, since it is not in the valid points! Recalculating...");
                        // Stop all courutines, maybe the path finding is still running
                        StopAllCoroutines();
                        setPath(layerMask);
                        // Start the new path from the beginning
                        mCurrentPathIndex = 1;
                        nextStep = Path[1];
                    }
                    if (nextStep == Checkpoints[mCurrentCheckpointIndex])
                        ++mCurrentCheckpointIndex;
                    startMovement(ConvertToVector3(nextStep));
                }
                break;
            default:
                break;
        }

    }
    #endregion

    #region private methods
    // TODO: setPath and getPath basically tha same, but setPath is not a coroutine.
    //       How to but them into one method still?
    private bool setPath(int layerMask)
    {
        Vector2 position = transform.position.ToVector2();
        Vector2 speed = mSpeed.ToVector2();
        LevelGenerator levelGenerator = LevelCreator.Instance.LevelGenerator;
        Node startNode = new Node() { Position = position, Speed = speed, LevelGenerator = levelGenerator, Parent = null };
        List<Node> nodes = new List<Node>();
        int nodeIndex = 0;

        Path = new List<Vector2>();
        nodes.Add(startNode);
        while (nodeIndex != nodes.Count)
        {
            List<Node> nexts = nodes[nodeIndex].GetNexts(layerMask);

            for(int i = 0; i < nexts.Count; ++i)
            {
                Node node = nexts[i];

                if (nodes.Contains(node))
                    continue;
                nodes.Add(node);
                if (node.Position == Checkpoints[mCurrentCheckpointIndex] && canStop(node, layerMask))
                {
                    // This will cause to exit the outer while loop
                    nodeIndex = nodes.Count - 1;
                    break;
                }
            }
            nodeIndex++;
        }
        for (Node node = nodes.Last(); node != null; node = node.Parent)
            Path.Add(node.Position);
        Path.Reverse();
        // If we could not reach the destination, we return false
        if (Path.Last() != Checkpoints[mCurrentCheckpointIndex])
            return false;
        // We need to start to build up the rest of the path. but that should be in a coroutine
        if(mCurrentCheckpointIndex < Checkpoints.Count - 1)
            StartCoroutine(getPath(mCurrentCheckpointIndex, mCurrentCheckpointIndex + 1, layerMask));
        return true;
    }

    private IEnumerator getPath(int fromIndex, int toIndex, int layerMask)
    {
        Vector2 speed = Path.Last() - Path.Last(1);
        LevelGenerator levelGenerator = LevelCreator.Instance.LevelGenerator;
        Node startNode = new Node() { Position = Checkpoints[fromIndex], Speed = speed, LevelGenerator = levelGenerator, Parent = null };
        List<Node> nodes = new List<Node>();
        int nodeIndex = 0;
        List<Vector2> path = new List<Vector2>();

        Debug.Log("Searching for path between: " + fromIndex + " and " + toIndex + "(" + Id + ")");
        nodes.Add(startNode);
        while (nodeIndex != nodes.Count)
        {
            List<Node> nexts = nodes[nodeIndex].GetNexts(layerMask);

            for(int i = 0; i < nexts.Count; ++i)
            {
                Node node = nexts[i];

                if (nodes.Contains(node))
                    continue;
                nodes.Add(node);
                // If we are heading towards the last checkpoint, there is no need to check, if we can stop
                if (node.Position == Checkpoints[toIndex] &&
                    (toIndex == Checkpoints.Count - 1 || canStop(node, layerMask)))
                {
                    nodeIndex = nodes.Count - 1;
                    break;
                }
            }
            yield return null;
            nodeIndex++;
        }
        for (Node node = nodes.Last(); node != null; node = node.Parent)
            path.Add(node.Position);
        path.Reverse();
        // The first element is not needed, since it is the checkpoint itself, and it is
        // already part of the path as the last step of the previous "sub-path".
        path.RemoveAt(0);
        Path.AddRange(path);
        // Start the next "sub-path" only if we managed to make this one
        if (Path.Last() == Checkpoints[toIndex])
        {
            Debug.Log("Path found between: " + fromIndex + " and "  + toIndex + "(" + Id + ")");
            if(toIndex != Checkpoints.Count - 1)
                StartCoroutine(getPath(toIndex, toIndex + 1, layerMask));
        }
    }

    bool canStop(Node node, int layerMask)
    {
        List<Node> nexts = node.GetNexts(layerMask);

        while (nexts.Count != 0)
        {
            float minSpeed = nexts.Min(n => n.Speed.sqrMagnitude);
            Node next = null;

            if (minSpeed == 0)
                return true;
            next = nexts.Find(n => n.Speed.sqrMagnitude == minSpeed);
            nexts = next.GetNexts(layerMask);
        }

        return false;
    }
    #endregion

    #region private members
    private List<Vector2> Checkpoints { set; get; }
    private float EnableTime  { set; get; }
    protected List<Vector2> Path { set; get; }
    private int mCurrentPathIndex = 0;
    private int mCurrentCheckpointIndex = 0;
    #endregion
}
