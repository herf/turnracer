﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(Button))]
public class LevelLoadingButton: MonoBehaviour
{
    public string LevelName = null;

    public void OnClick()
    {
        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(LevelName, LoadSceneMode.Single);
        Text text = GetComponentInChildren<Text>();

        if (null != text)
            text.text = "Loading...";
        loadOperation.allowSceneActivation = false;
        StartCoroutine(CheckProgress(loadOperation));
    }

    private IEnumerator CheckProgress(AsyncOperation operation)
    {
        while (operation.progress != 0.9f)
        {
            yield return new WaitForSeconds(0.1f);
        }
        operation.allowSceneActivation = true;
    }
}
