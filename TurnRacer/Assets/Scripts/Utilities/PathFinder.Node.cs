﻿using UnityEngine;
using System;

public partial class PathFinder
{
    private class Node : IEquatable<Node>, IComparable<Node>
    {
        public static Node create(Vector2 position, Vector2 destination, Node parent, LevelGenerator levelGenerator)
        {
            Node node = new Node();

            node.Position = position;
            node.H = (position - destination).sqrMagnitude;
            //node.H = (position - destination).magnitude;
            node.Parent = parent;
            if (null == parent)
            {
                node.G = 0;
            }
            else
            {
                //node.G = parent.G + 1;
                // With this formula it will prefer straight pathes over diagonal ones
                node.G = parent.G + (position - parent.Position).sqrMagnitude * 2;
            }
            // Calculate the N value, which is the number of "bad neighbours"
            // It is basically the number of not valid neighbours.
            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    int x = (int)position.x + i;
                    int y = (int)position.y + j;

                    if (!levelGenerator[x, y])
                        node.N += 1;
                }
            }

            return node;
        }

        public int CompareTo(Node other)
        {
            if (other == null)
                return 1;
            // If our F is smaller than other's, we will return a negativ value
            // meaning we should be before in the list
            //return this.F - other.F;
            return (int)(F - other.F);
        }

        public override bool Equals(object obj)
        {
            Node node = null;

            if (obj == null)
                return false;
            node = obj as Node;
            if (node == null)
                return false;
            else
                return Equals(node); // IT should throw an exception. Is it ever caloled at all?
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


        public bool Equals(Node other)
        {
            throw new NotImplementedException();
        }

        public float G { get; private set; }
        public float H { get; private set; }
        public float N { get; private set; }
        public Vector2 Position { get; private set; }
        public Node Parent { get; private set; }
        public float F
        {
            get { return H + G + N; }
        }
    }
}
