﻿using UnityEngine;
using System.Collections;

public class QuitApplicationButton: MonoBehaviour
{
    public void OnClick()
    {
        Application.Quit();
    }
}
