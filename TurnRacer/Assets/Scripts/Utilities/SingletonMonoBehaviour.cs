﻿using UnityEngine;

// TODO: Would be great to have Singletons on "scene" base, meaning each type can only have one instance per scene

/// <summary>
/// Base class for creating singleton MonoBehavious classes
/// It makes sure, that there is always only one instance with type "T"
/// T is a descendant class of SingletonMonoBehaviour
/// </summary>
/// <typeparam name="T">The type of the singleton class</typeparam>
public abstract class SingletonMonoBehaviour<T>: MonoBehaviour where T:SingletonMonoBehaviour<T>
{
    /// <summary>
    /// Getter to reach the only instance
    /// </summary>
    /// <returns>The only instance if there is any</returns>
    public static T Instance { get; private set; }

    /// <summary>
    /// Standard Unity Awake function
    /// </summary>
    void Awake()
    {
        if(Instance != default(T))
        {
            Debug.LogWarning("An instance is already created for " + typeof(T).Name +
                             " destroying this one (" + gameObject.name + ")!");
            mToBeDestroyed = true;
            Destroy(gameObject);

            return;
        }
        Instance = getSingletonInstance();
        onAwake();
    }

    /// <summary>
    /// Standard Unity OnDestroy function
    /// Used to make sure we are informed if the object is gone
    /// so an other one can be created
    /// </summary>
    void OnDestroy()
    {
        // If mTobeDestroyed is set, it means we deleted ourself
        // because this object is not the first one, so we have nothing
        // to do here
        if (mToBeDestroyed)
        {
            onDestroy();
            return;
        }
        Instance = default(T);
        onDestroy();
    }

    /// <summary>
    /// This method must be implemented by descendant classes
    /// so they can act upon the Awake event from Unity
    /// </summary>
    protected abstract void onAwake();
    /// <summary>
    /// This method must be implemented by descendant classes
    /// so they can act upon the OnDestroy event from Unity
    /// </summary>
    protected abstract void onDestroy();
    /// <summary>
    /// This method must be implemented by descendant classes
    /// so the only instance can be fetched
    /// </summary>
    protected abstract T getSingletonInstance();
    /// <summary>
    /// True if we ourselves decided to delete the object
    /// </summary>
    private bool mToBeDestroyed = false;
}
