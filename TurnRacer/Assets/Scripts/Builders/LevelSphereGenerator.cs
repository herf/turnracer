﻿using UnityEngine;
using System.Collections.Generic;

public class LevelSphereGenerator
{
	public LevelSphereGenerator(LevelSphereController sphere)
    {
        mLevelGenerator = LevelCreator.Instance.LevelGenerator;
        mSphereController = sphere;
        mSpheres = new Dictionary<Vector3, LevelSphereController>();
        createSpheres();
    }

    public bool enableLight(Vector3 position, Color color)
    {
        position.y = -1;
        if(!mSpheres.ContainsKey(position))
            return false;
        mSpheres[position].enableLight(color);

        return true;
    }

    public void disableLight(Vector3 position)
    {
        position.y = -1;
        if (!mSpheres.ContainsKey(position))
            return;
        mSpheres[position].disableLight();
    }

    public void highlight(Vector3 position)
    {
        position.y = -1;
        if (!mSpheres.ContainsKey(position))
            return;
        mSpheres[position].highlight();
    }

    public void unhighlight(Vector3 position)
    {
        position.y = -1;
        if (!mSpheres.ContainsKey(position))
            return;
        mSpheres[position].unhighlight();
    }

    private void createSpheres()
    {
        int size = mLevelGenerator.LevelSize;
        var parent = new GameObject();

        parent.transform.position = Vector3.zero;
        parent.name = "Parent";
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; j++)
            {
                if (mLevelGenerator[i, j])
                {
                    var position = new Vector3(i, -1, j);
                    var sphere = GameObject.Instantiate(mSphereController);

                    sphere.transform.position = position;
                    sphere.transform.parent = parent.transform;
                    mSpheres.Add(position, sphere);
                }
            }
        }
        StaticBatchingUtility.Combine(parent);
    }

    private LevelGenerator mLevelGenerator = null;
    private LevelSphereController mSphereController;
    private Dictionary<Vector3, LevelSphereController> mSpheres;
}
