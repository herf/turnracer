﻿using UnityEngine;
using UnityEngine.UI;
using uCPf;
using System;

public class SettingsPanelController: MonoBehaviour
{
    public int Id { get; set; }

    public void OnColorChange(Color color)
    {
        if (null != ConfigurationManager.Instance)
            ConfigurationManager.Instance.Colors[Id] = color;
    }

    public void OnNameChange(string name)
    {
        // LevelLoadingButton must have a Button component, it requires it
        StartButton.GetComponentInChildren<Button>().interactable = !string.IsNullOrEmpty(name);
        if (null != ConfigurationManager.Instance)
            ConfigurationManager.Instance.Names[Id] = name;
    }

    public void OnTypeChange(int type)
    {
        ConfigurationManager.Type typeEnum = (ConfigurationManager.Type)type;

        Activate(typeEnum);
        if (null != ConfigurationManager.Instance)
        {
            if (IsDefaultName(ConfigurationManager.Instance.Types[Id]))
            {
                // This will trigger "OnNameChange"
                NameInputField.text = GetDefaultName(typeEnum);
            }
            if (IsDefaultColor(ConfigurationManager.Instance.Types[Id]))
            {
                _colorPicker.color = GetDefaultColor(typeEnum);
                // We need to receive Unity events to update the GUI
                // and the "OnColorChange" will be triggered
                this.enabled = true;
            }
            ConfigurationManager.Instance.Types[Id] = typeEnum;
        }
    }

    // Use this for initialization
    private void Start()
    {
        ConfigurationManager.Type type = ConfigurationManager.Type.NONE;

        Id = (s_NextId++ % 4);
        _colorPicker = GetComponentInChildren<ColorPicker>();
        NameInputField = GetComponentInChildren<InputField>();
        TypeDropDown = GetComponentInChildren<Dropdown>();
        StartButton = transform.parent.GetComponentInChildren<LevelLoadingButton>();
        if (null == _colorPicker || null == NameInputField || null == TypeDropDown || null == StartButton)
        {
            Debug.LogWarning("Could not initialize SettingsPanel with id: " + Id);
            Destroy(gameObject);
        }
        type = (ConfigurationManager.Type)TypeDropDown.value;
        Activate(type);
        NameInputField.text = GetDefaultName(type);
        _colorPicker.color = GetDefaultColor(type);
    }

    private void LateUpdate()
    {
        ConfigurationManager instance = ConfigurationManager.Instance;

        _colorPicker.UpdateUI();
        // Storing the default values
        instance.Names[Id] = NameInputField.text;
        instance.Colors[Id] = _colorPicker.color;
        instance.Types[Id] = (ConfigurationManager.Type)TypeDropDown.value;
        // Do not receive this event anymore
        this.enabled = false;
    }

    private void Activate(ConfigurationManager.Type type)
    {
        NameInputField.interactable = ConfigurationManager.Type.NONE != type;
    }

    private bool IsDefaultName(ConfigurationManager.Type type)
    {
        string defaultName = GetDefaultName(type);

        return NameInputField.text == defaultName;
    }

    private string GetDefaultName(ConfigurationManager.Type type)
    {
        string defaultName = null;

        switch (type)
        {
            case ConfigurationManager.Type.PLAYER:
                defaultName = "Player " + Id;
                break;
            case ConfigurationManager.Type.AI:
                defaultName = "Enemy " + Id;
                break;
            case ConfigurationManager.Type.NONE:
                defaultName = "None " + Id;
                break;
            default:
                break;
        }

        return defaultName;
    }

    private bool IsDefaultColor(ConfigurationManager.Type type)
    {
        Color defaultColor = GetDefaultColor(type);

        return _colorPicker.color == defaultColor;
    }

    private Color GetDefaultColor(ConfigurationManager.Type type)
    {
        Color defaultColor = new Color32(0, 0, 0, byte.MaxValue);

        switch (type)
        {
            case ConfigurationManager.Type.PLAYER:
                defaultColor = new Color32(0, 200, 0, byte.MaxValue);
                break;
            case ConfigurationManager.Type.AI:
                defaultColor = new Color32(200, 0, 0, byte.MaxValue);
                break;
            default:
                break;
        }

        return defaultColor;
    }

    private static int s_NextId = 0;
    private ColorPicker _colorPicker = null;
    private InputField NameInputField { get; set; }
    private Dropdown TypeDropDown { get; set; }
    private LevelLoadingButton StartButton { get; set; }
}
