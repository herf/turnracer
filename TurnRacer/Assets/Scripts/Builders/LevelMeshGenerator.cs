﻿using UnityEngine;

public class LevelMeshGenerator
{
    public Mesh LevelMesh { get; private set; }

    public LevelMeshGenerator()
    {
        mLevelGenerator = LevelCreator.Instance.LevelGenerator;
        generateMesh();
    }
    
    private void generateMesh()
    {
        LevelMesh = new Mesh();
        LevelMesh.vertices = generateVertices();
        LevelMesh.triangles = generateTriangles();
        LevelMesh.normals = generateNormals();
        LevelMesh.uv = generateUvs();
        LevelMesh.name = "DynamicMesh";
        buildLevel();
    }

    private Vector3[] generateVertices()
    {
        int size = mLevelGenerator.LevelSize;
        int index = 0;
        var vertices = new Vector3[size * size];

        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
                vertices[index++] = new Vector3(i, 0, j);
        }

        return vertices;
    }

    private Vector3[] generateNormals()
    {
        int size = mLevelGenerator.LevelSize;
        int index = 0;
        var normals = new Vector3[size * size];

        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
                normals[index++] = Vector3.up;
        }

        return normals;
    }

    private int[] generateTriangles()
    {
        int size = mLevelGenerator.LevelSize;
        int index = 0;
        // The number of triangles is twice the number of faces
        // And the number of faces is the square of the size minus one.
        // If we have nine vertices (3x3) they form 4 faces (2x2)
        var triangles = new int[(size - 1) * (size - 1) * 2 * 3];

        for (int i = 0; i < size - 1; ++i)
        {
            for (int j = 0; j < size - 1; ++j)
            {
                int vertexIndex = (i * size) + j;

                // We have one face with the following vertex indices:
                // vertexIndex
                // vertexIndex + 1 -> The next one
                // vertexIndex + size -> The one "below"
                // vertexIndex + size + 1 -> The "opposite corner"
                // Create the first triangle
                triangles[index++] = vertexIndex;
                triangles[index++] = vertexIndex + 1;
                triangles[index++] = vertexIndex + size;
                // Create the second triengle
                triangles[index++] = vertexIndex + 1;
                triangles[index++] = vertexIndex + size + 1;
                triangles[index++] = vertexIndex + size;
            }
        }

        return triangles;
    }

    private Vector2[] generateUvs()
    {
        int size = mLevelGenerator.LevelSize;
        float uvStep = 1.0f / (size - 1);
        int index = 0;
        var uvs = new Vector2[size * size];

        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
                uvs[index++] = new Vector2(i * uvStep, j * uvStep);
        }

        return uvs;
    }

    private void buildLevel()
    {
        int size = mLevelGenerator.LevelSize;
        var vertices = LevelMesh.vertices;
        int index = 0;

        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                LevelGenerator.Level level = mLevelGenerator.GetLevelAt(i, j);

                if (LevelGenerator.Level.ROAD == level)
                    vertices[index].y -= 1;
                else if (LevelGenerator.Level.HOLE == level)
                    vertices[index].y -= 2;
                ++index;
            }
        }
        LevelMesh.vertices = vertices;
    }

    private LevelGenerator mLevelGenerator = null;
}
