﻿using UnityEngine;

// TODO: Start rotating the camera, if the user does not do anything for e.g. 10 seconds
public class CameraController: GameObjectMover
{
    public void RotateAround(float duration)
    {
        TurnDone += (sender, args) => Rotate(duration);
        Rotate(duration);
    }

    public void Move(VehicleController controller, float movementSpeed, float turningSpeed)
    {
        float distance = (transform.position - controller.transform.position).magnitude;
        float angle = Vector3.Angle(transform.forward, controller.transform.forward);
        float movementDuration = distance / movementSpeed;
        float turningDuration = angle / turningSpeed;

        Move(controller, Mathf.Max(movementDuration, turningDuration));
    }

    public void Move(VehicleController controller, float duration)
    {
        transform.parent = controller.transform;
        Transform(controller.transform.position, controller.transform.forward, duration, Space.World);
    }

    private void Rotate(float duration)
    {
        // Here we rotate only a 1/4 of a full circle to make sure we are always rotating
        // in one direction.
        Turn(Quaternion.Euler(0, 90, 0) * transform.forward, duration / 4, Space.World);
    }
}
