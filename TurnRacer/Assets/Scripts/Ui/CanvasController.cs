﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CanvasController : SingletonMonoBehaviour<CanvasController>
{
    public Slider[] Sliders = null;
    public Text[] Texts = null;
    public GameObject MessagePanel = null;

    public void TurnManagerCreated(TurnManager manager)
    {
        int index = GetIndex(manager.Id);
        int id = manager.Id;

        manager.TurnStarted += TurnStarted;
        manager.TurnEnded += TurnEnded;
        manager.VehicleChrashed += VehicleChrashed;
        manager.CheckpointReached += CheckpointReached;

        Texts[index].enabled = true;
        Texts[index].text = ConfigurationManager.Instance.Names[id];
        ChangeSliderColor(index, ConfigurationManager.Instance.Colors[id]);
    }

    public void GameManagerCreated(GameManager manager)
    {
        MessagePanel.SetActive(false);
        manager.RaceEnded += RaceEnded;
    }

    protected override CanvasController getSingletonInstance()
    {
        return this;
    }

    protected override void onAwake()
    {
        // Nothing to do
    }

    protected override void onDestroy()
    {
        // Nothing to do
    }

    private void Update()
    {
        if( Input.GetKeyDown(KeyCode.Escape))
        {
            if (MessagePanel.activeInHierarchy)
                MessagePanel.SetActive(false);
            else
                ShowMessagePanel("Paused");
        }
    }

    private void CheckpointReached(object sender, System.EventArgs eventArgs)
    {
        TurnManager manager = sender as TurnManager;
        int index = 0;
        float value = 0;

        if (null == manager)
            return;
        index = GetIndex(manager.Id);
        value = Sliders[index].value;
        value += 0.25f;
        Sliders[index].value = value;
    }

    private void TurnStarted(object sender, System.EventArgs eventArgs)
    {
        TurnManager manager = sender as TurnManager;

        if (null == manager)
            return;
        Texts[GetIndex(manager.Id)].fontStyle = FontStyle.Bold;
    }

    private void TurnEnded(object sender, System.EventArgs eventArgs)
    {
        TurnManager manager = sender as TurnManager;

        if (null == manager)
            return;
        Texts[GetIndex(manager.Id)].fontStyle = FontStyle.Normal;
    }

    private void VehicleChrashed(object sender, System.EventArgs eventArgs)
    {
        const byte rgb = 181;
        TurnManager manager = sender as TurnManager;
        int index = 0;

        if (null == manager)
            return;
        index = GetIndex(manager.Id);
        Texts[index].color = new Color32(rgb, rgb, rgb, byte.MaxValue);
        ChangeSliderColor(index, null);
    }

    private void RaceEnded(object sender, GameManager.RaceEndedEventArgs eventArgs)
    {
        string message = "You Won!!!";

        if (!eventArgs.PlayerWon)
            message = "You Lost!!!";
        ShowMessagePanel(message);
    }

    private void ShowMessagePanel(string message)
    {
        Text text = null;

        MessagePanel.SetActive(true);
        text = MessagePanel.transform.FindChild("MessageText").GetComponent<Text>();
        text = MessagePanel.GetComponentInChildren<Text>();
        if (null == text)
        {
            MessagePanel.SetActive(false);

            return;
        }
        text.text = message;
    }

    private void ChangeSliderColor(int index, Color? color = null)
    {
        // TODO: This is quite specific. Some kind of data binding would be better
        Image image = Sliders[index].transform.FindChild("Fill Area").GetChild(0).GetComponent<Image>();

        if (null != color)
        {
            image.color = color.Value;
        }
        else
        {
            float greyScale = image.color.grayscale;

            image.color = new Color(greyScale, greyScale, greyScale);
        }
    }

    private int GetIndex(int id)
    {
        if (!IdToIndexMap.ContainsKey(id))
            IdToIndexMap.Add(id, IdToIndexMap.Count);
        return IdToIndexMap[id];
    }

    private IDictionary<int, int> IdToIndexMap = new Dictionary<int, int>();
}
