﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class GameManager
{
    public class RaceEndedEventArgs: EventArgs
    {
        public bool PlayerWon { get; set; }
    }

    public event EventHandler<RaceEndedEventArgs> RaceEnded;

    public GameManager(UserVehicleController userVehicle, AiVehicleController aiVehicle,
                       CheckpointController checkpoint, CameraController cameraController)
    {
        LevelGenerator levelGenerator = LevelCreator.Instance.LevelGenerator;
        LevelSphereGenerator levelSphereGenerator = LevelCreator.Instance.LevelSphereGenerator;
        // The stup is to have the startingpoint, and the checkpoints ordered in clockwise
        Vector3 startPoint = levelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_LEFT);
        List<Vector3> checkpoints = new List<Vector3>
        {
            levelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_LEFT),
            levelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_RIGHT),
            levelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_RIGHT)
        };

        CameraController = cameraController;
        CameraController.TransformDone += (sender, eventArgs) => TurnManagers[CurrentManager].StartTurn();
        TurnManagers = new List<TurnManager>(4);
        // The setup is now OK for the Vehicle int the BOTTOM_LEFT part
        CreateTurnManager(userVehicle, aiVehicle, checkpoint, levelSphereGenerator, startPoint, checkpoints);
        // Here we move to the next AiVehicle, by moving the startPoint to the end of the checkpoints,
        // and set the first checkpoint to the startPoint. By this we simply moving to the next part
        // (TOP_LEFT) in clockwise order
        checkpoints.Add(startPoint);
        startPoint = checkpoints[0];
        checkpoints.RemoveAt(0);
        CreateTurnManager(userVehicle, aiVehicle, checkpoint, levelSphereGenerator, startPoint, checkpoints);
        checkpoints.Add(startPoint);
        startPoint = checkpoints[0];
        checkpoints.RemoveAt(0);
        CreateTurnManager(userVehicle, aiVehicle, checkpoint, levelSphereGenerator, startPoint, checkpoints);
        checkpoints.Add(startPoint);
        startPoint = checkpoints[0];
        checkpoints.RemoveAt(0);
        CreateTurnManager(userVehicle, aiVehicle, checkpoint, levelSphereGenerator, startPoint, checkpoints);
        CanvasController.Instance.GameManagerCreated(this);
    }

    public void StartGame()
    {
        // TODO: Some delay would be nice, before we start the camera movement between the turns
        CameraController.Move(TurnManagers[CurrentManager].GetVehicleController(),
                               movementSpeed: 5, turningSpeed: 45);
    }

    private void CreateTurnManager(UserVehicleController userVehicle, AiVehicleController aiVehicle,
                                   CheckpointController checkpoint, LevelSphereGenerator levelSphereGenerator,
                                   Vector3 startPoint, List<Vector3> checkpoints)
    {
        if (ConfigurationManager.Instance.Types[CurrentId] != ConfigurationManager.Type.NONE)
        {
            VehicleController controller = aiVehicle;

            if (ConfigurationManager.Instance.Types[CurrentId] == ConfigurationManager.Type.PLAYER)
                controller = userVehicle;
            CreateTurnManager(controller, checkpoint, startPoint, checkpoints.ToArray(), levelSphereGenerator);
        }
        else
        {
            // If no TurnManager is created, we still need to increase the ID, so next time we will check for the next value
            ++CurrentId;
        }
    }

    private void CreateTurnManager(VehicleController vehicle, CheckpointController checkpoint,
                                   Vector3 startPoint, Vector3[] checkpoints, LevelSphereGenerator levelSphereGenerator)
    {
        TurnManager turnManager = new TurnManager(CurrentId++, checkpoint, vehicle);

        turnManager.Initialize(startPoint, checkpoints);
        turnManager.TurnEnded += TurnEnded;
        turnManager.RaceEnded += TurnRaceEnded;
        turnManager.VehicleChrashed += VehicleChrashed;
        TurnManagers.Add(turnManager);
    }

    private void TurnEnded(object sender, EventArgs eventArgs)
    {
        if (_raceEnded)
            return;
        // If we reached the en dof the list, go back to the beginning,
        // which is index -1 in this case, since it will be increased to 0.
        if (CurrentManager >= TurnManagers.Count - 1)
            CurrentManager = -1;
        CameraController.Move(TurnManagers[++CurrentManager].GetVehicleController(),
                               movementSpeed: 10, turningSpeed: 90);
    }

    private void TurnRaceEnded(object sender, EventArgs eventArgs)
    {
        RaceEndedEventArgs raceEndedEventArgs = new RaceEndedEventArgs();
        TurnManager manager = sender as TurnManager;

        Debug.LogWarning("GameManager.raceEnded()");
        if (null == RaceEnded || null == manager)
            return;
        // Check if the player is the winner or not
        if (null != (manager.GetVehicleController() as UserVehicleController))
            raceEndedEventArgs.PlayerWon = true;
        else
            raceEndedEventArgs.PlayerWon = false;
        SendRaceEndedEvent(raceEndedEventArgs);
    }

    private void VehicleChrashed(object sender, EventArgs eventArgs)
    {
        TurnManager manager = sender as TurnManager;

        // Sanity check
        if (null == manager || TurnManagers[CurrentManager] != manager)
        {
            Debug.LogError("Internal logic error: Received vehicleCrashed callback on an other TurnManager than the current!");

            return;
        }
        TurnManagers.Remove(manager);
        // After removing one manager, we need to step back the index of the current one
        --CurrentManager;
        // If one of the player chrashed
        if (manager.GetVehicleController() is UserVehicleController)
        {
            bool isMorePlayers = false;

            foreach (var turnManager in TurnManagers)
            {
                if(turnManager.GetVehicleController() is UserVehicleController)
                {
                    isMorePlayers = true;
                    break;
                }
            }
            // TODO: Rotate the camera around the player once, before continue the game
            // It is only game over, if there are no more players left
            if (!isMorePlayers)
            {
                manager.KeepOn = true;
                SendRaceEndedEvent(new RaceEndedEventArgs() { PlayerWon = false });
            }
        }
        // There will be a TurnEnded event after this event
    }

    private void SendRaceEndedEvent(RaceEndedEventArgs eventArgs)
    {
        _raceEnded = true;
        // We need to unparent the camera, since the Vehicle it is parented to, is going to be destroyed
        CameraController.transform.parent = null;
        CameraController.RotateAround(5);
        if (null == RaceEnded)
            return;
        RaceEnded(this, eventArgs);
    }

    private List<TurnManager> TurnManagers { get; set; }
    // The index of the currently handled TurnManager in the list
    private int CurrentManager { get; set; }
    private int CurrentId { get; set; }
    private CameraController CameraController { get; set; }
    private bool _raceEnded = false;
}
