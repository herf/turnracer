﻿using UnityEngine;
using System.Collections;

public class UserVehicleController: VehicleController
{
    public Mesh HoloMesh = null;

	private void Start()
    {
        // This has to be done here, to make sure, the CameraController is attached itself to us
        _camera = GetComponentInChildren<Camera>();
	}

    public override bool init(int id, Vector3 startPosition, Vector3[] checkpoints)
    {
        MeshFilter meshFilter = GetComponentInChildren<MeshFilter>();

        // Sanity check
        if(!base.init(id, startPosition, checkpoints) || null == meshFilter)
            return false;
        OriginalMesh = meshFilter.mesh;

        return true;
    }

    private void Update()
    {
        switch(mState)
        {
            case State.IDLE:
                turnOnSpheres();
                HighLightSphere();
                if (Input.GetButtonDown("Fire1") && PreviousController != null)
                {
                    Vector3 newPosition = PreviousController.transform.position;

                    // The user wants to move
                    SphereUnselected();
                    turnOffSpheres();
                    newPosition.y = transform.position.y;
                    startMovement(newPosition);
                }
                break;
            default:
                break;
        }
    }

    private void HighLightSphere()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        // Everything but the veicles and the checkpoints
        int layerMask = ~(1 << 8 | 1 << 9);

        // The user is pointing on something
        if (Physics.Raycast(ray, out hit, float.PositiveInfinity, layerMask))
        {
            LevelSphereController controller = null;

            // Check if there is a LevelSphereController attached to the GameObject
            if (null != hit.collider)
                controller = hit.collider.GetComponent<LevelSphereController>();
            if (null != controller)
            {
                if (controller.isAvailable())
                {
                    SphereSelected(controller);
                }
            }
        }
    }

    private void SphereSelected(LevelSphereController controller)
    {
        // First reset everything back to normal if needed
        SphereUnselected();
        // Change the mesh if needed
        SetMesh(controller);
        // Stor the controller for later use, and highlight it
        PreviousController = controller;
        controller.highlight();
    }

    private void SphereUnselected()
    {
        // If nor previously selcted spere, there is nothing to do
        if (null == PreviousController)
            return;
        // Set the mesh - without the parameter it will set it to the original one
        SetMesh();
        // Reset the previous controller
        PreviousController.unhighlight();
        PreviousController = null;
    }

    private void SetMesh(LevelSphereController controller = null)
    {
        Renderer renderer = GetComponentInChildren<Renderer>();
        MeshFilter meshFilter = GetComponentInChildren<MeshFilter>();

        if (null == renderer || null == meshFilter)
            return;
        renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        meshFilter.mesh = OriginalMesh;
        // If no controller set, we are done
        if (null == controller)
            return;
        // If the selected controller is right below us, set the holo mesh and material
        if (controller.transform.position.x == transform.position.x &&
            controller.transform.position.z == transform.position.z)
        {
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            meshFilter.mesh = HoloMesh;
        }
    }

    private Camera _camera = null;
    private LevelSphereController PreviousController { get; set; }
    private Mesh OriginalMesh { get; set; }
}
