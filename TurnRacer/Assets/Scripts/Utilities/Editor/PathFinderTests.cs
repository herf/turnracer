﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System.Collections.Generic;

public class PathFinderTests
{
    /*public class TestLevelGenerator: LevelGenerator
    {
        public TestLevelGenerator(): base(Size.SMALL)
        {
            // Nothing to do
        }

        public override bool[,] getLevelData()
        {
            return LevelData;
        }

        public bool[,] LevelData { get; set; }
    }

    [Test]
    public void EditorTest()
    {
        //Arrange
        var gameObject = new GameObject();

        //Act
        //Try to rename the GameObject
        var newGameObjectName = "My game object";
        gameObject.name = newGameObjectName;

        //Assert
        //The object has a new name
        Assert.AreEqual(newGameObjectName, gameObject.name);
    }

    [Test]
    public void TestValidPath()
    {
        // Arrange
        bool[,] testLevel =
            {
                { false, false, false, false, false, false, false, false, false },
                { false, true,  true,  true,  true,  true,  true,  true,  false },
                { false, true,  true,  true,  true,  true,  true,  true,  false },
                { false, true,  true,  true,  false, false, true,  true,  false },
                { false, true,  true,  true,  false, false, true,  true,  false },
                { false, false, false, false, false, false, false, false, false }

            };
        LevelGenerator levelGenerator = new TestLevelGenerator() { LevelData = testLevel };
        PathFinder pathFinder = new PathFinder(levelGenerator);
        Vector2 from = new Vector2(3, 1);
        Vector2 to = new Vector2(1, 7);

        // Act
        List<Vector2> path = pathFinder.getPath(from, to);

        // Assert
        Assert.AreEqual(from, path[0]);
        Assert.AreEqual(to, path[path.Count - 1]);
    }

    [Test]
    public void TestInvalidPath()
    {
        // Arrange
        bool[,] testLevel =
            {
                { false, false, false, false, false, false, false, false, false },
                { false, true,  true,  true,  true,  true,  false, true,  false },
                { false, true,  true,  true,  true,  false, true,  true,  false },
                { false, true,  true,  true,  false, false, true,  true,  false },
                { false, true,  true,  true,  false, false, true,  true,  false },
                { false, false, false, false, false, false, false, false, false }

            };
        LevelGenerator levelGenerator = new TestLevelGenerator() { LevelData = testLevel };
        PathFinder pathFinder = new PathFinder(levelGenerator);
        Vector2 from = new Vector2(3, 1);
        Vector2 to = new Vector2(1, 7);

        // Act
        List<Vector2> path = pathFinder.getPath(from, to);

        // Assert
        Assert.AreEqual(0, path.Count);
    }

    [Test]
    public void TestShortestPath()
    {
        // Arrange
        bool[,] testLevel =
            {
                { false, false, false, false, false, false, false, false, false },
                { false, true,  true,  true,  true,  true,  true,  true,  false },
                { false, true,  true,  true,  false, false, true,  true,  false },
                { false, true,  true,  true,  false, false, true,  true,  false },
                { false, true,  true,  true,  false, false, true,  true,  false },
                { false, false, false, false, false, false, false, false, false }

            };
        LevelGenerator levelGenerator = new TestLevelGenerator() { LevelData = testLevel };
        PathFinder pathFinder = new PathFinder(levelGenerator);
        Vector2 from = new Vector2(1, 1);
        Vector2 to = new Vector2(3, 7);

        // Act
        List<Vector2> path = pathFinder.getPath(from, to);

        // Assert
        Assert.AreEqual(from, path[0]);
        Assert.AreEqual(to, path[path.Count - 1]);
    }*/
}
