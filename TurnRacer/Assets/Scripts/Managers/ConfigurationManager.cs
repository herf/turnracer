﻿using UnityEngine;
using System.Collections;
using System;

public class ConfigurationManager : SingletonMonoBehaviour<ConfigurationManager>
{
    public enum Type
    {
        PLAYER,
        AI,
        NONE,
    }

    public string[] Names = new string[4];
    public Color[] Colors = new Color[4];
    public Type[] Types = new Type[4];

    protected override ConfigurationManager getSingletonInstance()
    {
        return this;
    }

    protected override void onAwake()
    {
        // Nothing to do yet
    }

    protected override void onDestroy()
    {
        // Nothing to do yet
    }

    private void Start()
    {
        DontDestroyOnLoad(this);
    }
}
