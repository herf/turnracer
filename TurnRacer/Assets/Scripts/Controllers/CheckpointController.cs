﻿using UnityEngine;
using System;

public class CheckpointController: MonoBehaviour
{
    public int Id { get; private set; }
    public event EventHandler CheckpointReached;

    public void init(int id, bool startPosiiton, Color color)
    {
        ProceduralMaterial material = GetComponent<Renderer>().material as ProceduralMaterial;
        Light light = GetComponentInChildren<Light>();

        Id = id;
        MeshRenderer = GetComponent<MeshRenderer>();
        // If this checkpoint is the startingpoint, then it will only be shown,
        // after the player has left the starting position
        MeshRenderer.enabled = !startPosiiton;
        if (null != material)
        {
            material.SetProceduralColor("Emissive_Color", color);
            material.RebuildTexturesImmediately();
        }
        if (null != light)
        {
            light.color = color;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        VehicleController controller = null;

        // If the mesh is not shown, we are the starting point,
        // so this is not the time to destroy ourselves        
        if (false == MeshRenderer.enabled)
            return;
        controller = collider.gameObject.GetComponent<VehicleController>();
        if (null == controller || Id != controller.Id)
            return;
        if (null != CheckpointReached)
            CheckpointReached(this, EventArgs.Empty);
        Destroy(gameObject);
    }

    private void OnTriggerExit(Collider collider)
    {
        var controller = collider.gameObject.GetComponent<VehicleController>();

        if (null == controller || Id != controller.Id)
            return;
        // The vehicle has left the starting point,
        // from now on we are just a usuall checkpoint
        MeshRenderer.enabled = true;
    }

    private MeshRenderer MeshRenderer { get; set; }
}
