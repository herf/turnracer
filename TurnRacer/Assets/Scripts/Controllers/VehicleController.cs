﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO: There is no real need to have two separate prefab for Ai and UserVehicles
//       We can create them on the fly, and add the proper script to the GameObject.
public class VehicleController: GameObjectMover
{
    public GameObject AfterBurner = null;
    public GameObject Explosion = null;
    public Color LightColor = Color.black;
    public event System.EventHandler TurnEnded;
    public event System.EventHandler VehicleChrashed;

    public static Vector3 ConvertToVector3(Vector2 vector)
    {
        return new Vector3(vector.x, -0.5f, vector.y);
    }

    public static bool BoxCast(Vector3 from, Vector3 distance, int layerMask)
    {
        // Half extent must be less than 0.25, otherwise the raycast will report the road spheres
        return Physics.BoxCast(from, Vector3.one / 5, distance.normalized,
                               Quaternion.identity, distance.magnitude, layerMask);
    }

    public int Id { get; private set; }

    public virtual bool init(int id, Vector3 startPosition, Vector3[] checkpoints)
    {
        ProceduralMaterial material = GetComponentInChildren<Renderer>().material as ProceduralMaterial;
        Vector4 offset = Vector4.zero;

        Id = id;
        transform.position = startPosition;
        if (null == material)
            return false;
        mLevelSphereGenerator = LevelCreator.Instance.LevelSphereGenerator;
        mValidPoints = new List<Vector2>(9);
        LightColor = ConfigurationManager.Instance.Colors[Id];
        transform.forward = checkpoints[0] - transform.position;
        TurnDone += (sender, eventArgs) => PreMove();
        MoveDone += (sender, eventArgs) => PostMove();
        // Set the light color and a random offset of the textures, so every vehicle look "unique"
        offset = new Vector4(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 0, 0);
        material.SetProceduralVector("Offset", offset);
        material.SetProceduralColor("Emission_Color", LightColor);
        material.RebuildTexturesImmediately();

        return true;
    }

    protected enum State
    {
        IDLE,
        TURNING,
        MOVING,
        EXPLODING,
    }

    protected void turnOnSpheres()
    {
        bool spaceToMove = false;
        Vector3 position = Vector3.zero;

        if (null == mLevelSphereGenerator || mSpheresOn == true)
            return;
        mValidPoints.Clear();
        position = transform.position + mSpeed;
        spaceToMove |= enableLight(position);
        spaceToMove |= enableLight(position + Vector3.forward);
        spaceToMove |= enableLight(position + Vector3.forward + Vector3.right);
        spaceToMove |= enableLight(position + Vector3.forward + Vector3.left);
        spaceToMove |= enableLight(position + Vector3.back);
        spaceToMove |= enableLight(position + Vector3.back + Vector3.right);
        spaceToMove |= enableLight(position + Vector3.back + Vector3.left);
        spaceToMove |= enableLight(position + Vector3.left);
        spaceToMove |= enableLight(position + Vector3.right);
        mSpheresOn = true;
        if (false == spaceToMove)
        {
            // There is nowhere to run, the vehicle crashed
            StartCoroutine(explode());
        }
    }

    protected bool enableLight(Vector3 position)
    {
        Vector3 distance = position - transform.position;
        // Everything but the checkpoints, since even vehicles cna block the way
        int layerMask = ~(1 << 9);

        // If there is something blocking the way to the position, it will not be highlighted
        if (BoxCast(transform.position, distance, layerMask))
            return false;
        // TODO: mValidPoints are handled in many different places and not in the right methods
        if (mLevelSphereGenerator.enableLight(position, LightColor))
        {
            mValidPoints.Add(position.ToVector2());

            return true;
        }

        return false;
    }

    protected void turnOffSpheres()
    {
        if (null == mLevelSphereGenerator)
            return;
        Vector3 position = transform.position + mSpeed;
        mLevelSphereGenerator.disableLight(position);
        mLevelSphereGenerator.disableLight(position + Vector3.forward);
        mLevelSphereGenerator.disableLight(position + Vector3.forward + Vector3.right);
        mLevelSphereGenerator.disableLight(position + Vector3.forward + Vector3.left);
        mLevelSphereGenerator.disableLight(position + Vector3.back);
        mLevelSphereGenerator.disableLight(position + Vector3.back + Vector3.right);
        mLevelSphereGenerator.disableLight(position + Vector3.back + Vector3.left);
        mLevelSphereGenerator.disableLight(position + Vector3.left);
        mLevelSphereGenerator.disableLight(position + Vector3.right);
        mSpheresOn = false;
    }

    protected void startMovement(Vector3 position)
    {
        mSpeed = position - transform.position;
        Turn();
    }

    private void Turn()
    {
        float duration = 0;

        if (Vector3.zero == mSpeed)
        {
            SendEvent(TurnEnded);

            return;
        }
        duration = Vector3.Angle(transform.forward, mSpeed) / 180;
        mState = State.TURNING;
        if (duration > 0)
            Turn(mSpeed, duration, space: Space.World);
        else
            PreMove();
    }

    private void PreMove()
    {
        mState = State.MOVING;
        if (null != AfterBurner)
        {
            mAfterBurner = Instantiate(AfterBurner);
            mAfterBurner.transform.parent = transform;
            mAfterBurner.transform.localRotation = Quaternion.identity;
            mAfterBurner.transform.Rotate(new Vector3(0, 180, 0));
            mAfterBurner.transform.position = transform.position + (-0.4f * transform.forward);

        }
        // Here the duration is fix, so the player can "feel" the speed
        Move(transform.position + mSpeed, 0.3f, Space.World);
    }

    private void PostMove()
    {
        Vector3 position = transform.position;

        // We need to round the coordinates to make sure  we found the right sphere
        position.x = Mathf.Round(position.x);
        position.z = Mathf.Round(position.z);
        transform.position = position;
        mState = State.IDLE;
        if (null != mAfterBurner)
            Destroy(mAfterBurner);
        SendEvent(TurnEnded);
    }

    private IEnumerator explode()
    {
        GameObject explosion = Instantiate(Explosion);

        // Change to EXPLODING state, so descendant classes will not do anything
        // while the explosion is playing.
        mState = State.EXPLODING;
        explosion.transform.position = transform.position;
        // The gameObject cannot be destroyed here, since the camera is still attached to it
        // We need to turn on the MeshRenderer and the Light component
        gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;
        gameObject.GetComponentInChildren<Light>().enabled = false;
        yield return new WaitForSeconds(3);
        // Send both a VehicleCrashed and a TurnEnded event - strictly in this order
        SendEvent(VehicleChrashed);
        SendEvent(TurnEnded);
        Destroy(explosion);
        Destroy(gameObject);
        mState = State.IDLE;
    }

    private void SendEvent(System.EventHandler eventHandler)
    {
        if (null != eventHandler)
            eventHandler(this, System.EventArgs.Empty);
    }

    protected State mState = State.IDLE;
    protected Vector3 mSpeed = Vector3.zero;
    protected List<Vector2> mValidPoints;
    private LevelSphereGenerator mLevelSphereGenerator = null;
    private bool mSpheresOn = false;
    private GameObject mAfterBurner;
}
