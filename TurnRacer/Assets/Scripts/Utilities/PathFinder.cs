﻿using UnityEngine;
using System;
using System.Collections.Generic;

public partial class PathFinder
{
    public PathFinder(LevelGenerator levelGenerator)
    {
        mLevelGenerator = levelGenerator;
    }

    public List<Vector2> getPath(Vector2 from, Vector2 to)
    {
        List<Node> openNodes = new List<Node>();
        List<Node> closedNodes = new List<Node>();
        List<Vector2> path = new List<Vector2>();

        openNodes.Add(Node.create(from, to, null, mLevelGenerator));
        while (true)
        {
            Node currentNode = null;
            Vector2 position = Vector2.zero;

            // If there is no more open node, we are done, 
            // and there is no path between from and to
            // Returning the empty list as a path will 
            // indicate the failure
            if (openNodes.Count == 0)
                return path;
            // TODO: This can maybe be better optimized - Use SortedSet
            //       Try not to sort every time. Keep the list sorted all the time
            //       Maybe List is not the best collection for this
            openNodes.Sort();
            currentNode = openNodes[0];
            position = currentNode.Position;
            closedNodes.Add(currentNode);
            openNodes.RemoveAt(0);
            // If we are at the destination, we are done,
            // we have found the path
            if (position == to)
                break;
            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    Vector2 currentPosition = position + new Vector2(i, j);
                    int closedIndex = closedNodes.FindIndex(n => n.Position == currentPosition);
                    int openedIndex = openNodes.FindIndex(n => n.Position == currentPosition);

                    // If the position is not walkable (not in the level) or
                    // it is already in the closed list, or
                    // it isnot a valid diagonal movement, just ignore it
                    if (!mLevelGenerator[currentPosition] || closedIndex != -1 ||
                        !isOkDiagonal(currentPosition, currentNode.Position))
                        continue;
                    if (openedIndex == -1)
                    {
                        openNodes.Add(Node.create(currentPosition, to, currentNode, mLevelGenerator));
                    }
                    else
                    {
                        Node node = Node.create(currentPosition, to, currentNode, mLevelGenerator);

                        if (node.G < openNodes[openedIndex].G)
                            openNodes[openedIndex] = node;
                    }
                }
            }
        }
        for (Node node = closedNodes[closedNodes.Count - 1]; node != null; node = node.Parent)
        {
            path.Add(node.Position);
        }
        path.Reverse();
        return path;
    }

    private bool isOkDiagonal(Vector2 position, Vector2 nodePosition)
    {
        Vector2 diff = position - nodePosition;

        // If any of the diff coordinates are zero, it is not a diagonal position
        if (0 == diff.x || 0 == diff.y)
            return true;
        // A diagonal movement is valid, if the neighbouring level cells are valid pathes also
        return mLevelGenerator[nodePosition + new Vector2(0, diff.y)] &&
               mLevelGenerator[nodePosition + new Vector2(diff.x, 0)];
    }

    private LevelGenerator mLevelGenerator = null;
}
