﻿using UnityEngine;
using System.Collections;

// TODO: Create a texture size slider
// TODO: Create a loading scene.

public class MainMenuCreator : LevelCreator
{
    // Use this for initialization
    void Start ()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();

        // Generate the level data
        Debug.Log("Generating level");
        LevelGenerator = new LevelGenerator(Size);
        // Generate the mesh, representing the level
        LevelMeshGenerator = new LevelMeshGenerator();
        meshFilter.mesh = LevelMeshGenerator.LevelMesh;
        gameObject.AddComponent<MeshCollider>();
        // Set up the substance and rebuild the textures immediately
        LevelMaterial.SetProceduralTexture("Mask", LevelGenerator.LevelMask);
        LevelMaterial.SetProceduralFloat("Tiling", LevelGenerator.Tiling);
        LevelSideMaterial.SetProceduralFloat("Tiling", LevelGenerator.Tiling);
        LevelMaterial.RebuildTexturesImmediately();
        meshRenderer.material = LevelMaterial;
        // Generate the spheres showing the path to the player
        LevelSphereGenerator = new LevelSphereGenerator(LevelSphere);
        PlaceFlags();
        PlaceParticle();
        PlaceVehicles();
        SetupCameraController();
        // Set the resolution to the final quality, but this time load it in the background
        LevelMaterial.SetProceduralVector("$outputsize", LevelGenerator.MaterialOutputSize);
        LevelMaterial.RebuildTextures();
        LevelSideMaterial.SetProceduralVector("$outputsize", LevelGenerator.MaterialOutputSize);
        LevelSideMaterial.RebuildTextures();
    }

    private void PlaceVehicles()
    {
        Vector3[] startingPoints =
        {
            LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_LEFT),
            LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_LEFT),
            LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.TOP_RIGHT),
            LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_RIGHT),
            // This is just to make the for() easier to implement
            LevelGenerator.GetStartingPoint(LevelGenerator.Quarter.BOTTOM_LEFT),
        };
        _vehicles = new VehicleController[4];

        for (int i = 0; i < 4; ++i)
        {
            VehicleController controller = null;

            if (0 == i)
                controller = Instantiate(UserVehicle);
            else
                controller = Instantiate(AiVehicle);
            controller.transform.position = startingPoints[i];
            controller.transform.forward = startingPoints[i + 1] - startingPoints[i];
            controller.enabled = false;
            _vehicles[i] = controller;
        }
    }

    private void SetupCameraController()
    {
        VehicleController controller = _vehicles[_currentVehicle];

        _cameraController = CreateCameraController();
        _cameraController.TransformDone += (sender, eventArgs) => StartCoroutine(WaitOneSecond());
        _cameraController.Move(controller, 5, 45);
    }

    private IEnumerator WaitOneSecond()
    {
        VehicleController controller = null;

        yield return new WaitForSeconds(1);
        ++_currentVehicle;
        if (4 == _currentVehicle)
            _currentVehicle = 0;
        controller = _vehicles[_currentVehicle];
        _cameraController.Move(controller, 5, 45);
    }

    private VehicleController[] _vehicles;
    private int _currentVehicle = 0;
    private CameraController _cameraController = null;
}
