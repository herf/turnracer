﻿using UnityEngine;
using System.Collections;

public class LevelSphereController: MonoBehaviour
{
    public void enableLight(Color color)
    {
        Light.color = color;
        Light.enabled = true;
    }

    public void disableLight()
    {
        Light.enabled = false;
    }

    public bool isAvailable()
    {
        return Light.enabled;
    }

    public void highlight()
    {
        Light.intensity *= 2;
    }

    public void unhighlight()
    {
        Light.intensity /= 2;
    }

    private void Start()
    {
        Light = gameObject.GetComponentInChildren<Light>();
    }

    private Light Light { set; get; }
}
